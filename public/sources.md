##### Introduction
- [Wikipedia - good overview and 4 Essential Freedoms](https://en.wikipedia.org/wiki/Free_and_open-source_software#FLOSS)

##### Philosophy
- [Left picture of the title page](https://opensource.com/article/17/11/open-source-or-free-software)

- [Right picture of the title page](https://www.gnu.org/philosophy/free-software-for-freedom.en.html)

- [Another paper that summarizes the pov of the FSF](https://www.gnu.org/philosophy/free-software-for-freedom.en.html)

- [About open source victory](https://opensource.com/business/16/11/open-source-not-free-software)
##### Business

- [Open Source License Selection in Relation to Business Models](https://timreview.ca/article/416)

- [LibreSelery](https://github.com/protontypes/LibreSelery)

- [Ethical NonTracking Ads](https://awesomeopensource.com/project/gitcoinco/code_fund_ads)

- [Patreon](https://www.patreon.com/)

- [Flattr](https://flattr.com/featured)

- [Ko-fi](https://ko-fi.com/)

- [Bountysource](https://www.bountysource.com/)

- [Outreachy](https://www.outreachy.org/)

- [Open Technology Fund](https://www.opentech.fund/funds/)

- [netidee](https://www.netidee.at/)


##### Licenses

- [Open Source Initiative - Licenses](https://opensource.org/licenses/category)

- [TL;DR Legal](https://tldrlegal.com/)

- [Choose A License](https://choosealicense.com/licenses/)

##### Issues in the community