##### Licenses

Licences set a scope in which you may use the work.
> Can ✔ - Cannot ❌ - Must ❕
  * Private use
  * Modification
  * Distribution
  * Sublicensing
  * Patent grant
  * Trademark grant