### Free software - A fight for freedom

Free software advocates have a strong will to protect the 4 Essential Freedom of the user.

* They fight against proprietary/closed-source software
* They fight against the DRM (Digital Restrictions Management), see defectivebydesign.org
* They fight against the term FLOSS or open-souce software