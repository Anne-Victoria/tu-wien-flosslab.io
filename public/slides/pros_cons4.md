##### For companies to implement
**Pros**
* Enlarges the potential users of the company
* The company is well thought of

**Cons**
- Harder to make profit (difficulty of creating a business model)