## Benevolent Dictatorship (leadership)​

- One person has overriding influence​

- Leader's opinion shapes project's quality​

- Leader's role may not be structural by socio-political​

- Often uses Maintainers which most review codes​

- May help avoid endless discussions​

- E.G. Linux, OpenBSD, Ethereum, WordPress
