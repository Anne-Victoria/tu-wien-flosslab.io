##### For individuals

**Pros**
* Often cost-free
* Better privacy and security
* Constantly improved for big software

**Cons**
* Often harder to handle (made for developpers)
* less applications 
* Quality of close source often greater than open source