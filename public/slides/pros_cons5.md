##### For programmers
**Pros**
* Discover new technologies and new methods
* Work with different communities
* Good for the CV

**Cons**
* Hard to get money from it (often work for free)
* Communities can be pretty rigid