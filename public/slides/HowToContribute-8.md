## So you so want to help 
### Work on Issues
- Start with issues tagged ***Good First Issue***
- Make sure the issue is not assigned
- You can work on your own issue
### Review Pull Requests
- Make sure you can do this - CONTRIBUTING
- Be sure of your review