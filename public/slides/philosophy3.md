### Open-source - Plural points of view

Open-source advocates are plural and they praise it for different reasons

* They think open source is a key to improve technology
* Some don't want to be included in the morals of free software
* Some think it's better in a marketing way than free software
* Many don't know the difference with free software
