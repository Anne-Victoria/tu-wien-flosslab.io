##### For companies to use
**Pros**
* Cost-free base that would have required tons of developers and time to make it

**Cons**
* Licenses can be too much restrictive (GNU)
* Not a full knowledge on the software
* Either fork and prevent the companies of good updates, either stay with the original project and lead to unwanted updates