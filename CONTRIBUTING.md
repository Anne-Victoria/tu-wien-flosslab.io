# Contributing
Would you like to contribute to this project? Great! We outlined our contribution flow in this file to help you get familiar with how we work. 

## Getting Started
Feel free to create your own issues and work on them or browse the existing ones and see if there is something you would like to do. To be able to contribute and take on issues, request access to this project and we will add you as developer. If you need help with getting the project running locally or with how to create new slides, please refer to the [README.md](https://gitlab.com/tu-wien-floss/tu-wien-floss.gitlab.io/-/blob/master/README.md).

## Project Chat
Our project communicates via Mattermost. If you would like to contribute, feel free to join our chat.
## Creating Issues
Have an idea for a new feature? Want to create slides about some super interesting topic? Found a bug? Feel free to create create an issue here on Gitlab! Just keep the following rules in mind:
- Search the existing issues first to make sure, that you are not creating a duplicate of an existing issue
- Add a short description and todo list to your issue
- If you have any resources that might be relevant, please link them
- Take a moment to label your issue with fitting labels e.g. "good first issue" or "javascript"

## Working on Issues
If you would like to work on an issue, please follow these steps:
1. Check that no one is working on this already. If the Issue is unassigned, feel free to grab it.
2. Assign the issue to yourself, so that everyone knows you are currently working on this.
3. Create a branch from the `master` branch for working on the issue. Please include the ticket-number in the name of the branch e.g. `#13-fix-missing-favicon`.
4. Checkout out your branch locally and work on the issue. Create commits as needed. Please follow the commit message guideline stated below.
5. When you are done with the issue and pushed all commits on the branch to Gitlab, you can create a merge request to merge your changes into `master`.
6. When your merge request has been approved and merge, you can close the issue.

## Merge Requests
When you are done working on something in a branch, you can create a merge request to the `master` branch. Ask someone to review your merge request. At least one person from another group needs to review and approve your changes in the merge request, before the maintainers will merge the merge request.

## Branches
The main branch of this repository is `master`. You should never commit anything directly on `master`. Instead please work in individual branches and create merge requests as needed. Branches should be named after the issue number and title e.g. `#13-fix-missing-favicon`.

## Commit Messages
Please stick to the following format when making a commit:

```
GroupName #IssueNumber: Title (less than 50 characters)

A short description of the issue (optional)
```

*The title should be of the imperative format, as if it were written in the sentence :*
"If accepted, this commit will [title]"

### Example

```
FLOSSGroup #3: Create file CONTRIBUTING.md

Define the rules to make a commit in this project.
```

## JavaScript Code Formatting
JavaScript code should be formatted using [prettier](https://prettier.io/).
